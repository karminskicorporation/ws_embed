#include "AM2320.h"
#include "twisup.h"
#include <stdbool.h>
#include <util/twi.h>

//AM2320 SLA 0x5C (7-bit)
#define AM2320_SLAW (0xB8)
#define AM2320_SLAR (0xB9)

int16_t AM2320_temperature = 0;
uint16_t AM2320_humidity = 0;


bool AM2320_get_all(){
	uint8_t i;
	
	union{
		uint8_t byte[8];
		struct{
			uint8_t CRC_MSB;
			uint8_t CRC_LSB;
			uint16_t temperature:15;
			bool is_minus:1;
			uint16_t humidity;
			uint8_t echo_comm[2];
		};
	}conv;
	
	//wake up procedure
	if(!twi_controller_start()) return false;
	twi_controller_send_byte(AM2320_SLAW);
	twi_controller_stop();
	
	//measurement
	if(!twi_controller_start() ||
	!twi_controller_send_sla(AM2320_SLAW) ||
	!twi_controller_send_byte(0x03) ||
	!twi_controller_send_byte(0x00) ||
	!twi_controller_send_byte(0x04))
	return false;
	
	twi_controller_stop();
	_delay_ms(2);
	
	if(!twi_controller_start() ||
	!twi_controller_send_sla(AM2320_SLAR))
	return false;
	
	_delay_us(50);
	for(i=0; i<8; i++)
		conv.byte[7-i] = twi_controller_get_byte(true);
	conv.byte[0] = twi_controller_get_byte(false);
	twi_controller_stop();
	
	//przeliczanie danych
	AM2320_temperature = (int16_t)(conv.temperature) * (conv.is_minus ? (-1) : 1);
	AM2320_humidity = conv.humidity;
	
	return true;
}
