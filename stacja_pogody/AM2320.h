#ifndef AM2330_H_
#define AM2330_H_
#include <avr/io.h>
#include <stdbool.h>
#include <util/delay.h>

#define AM2320_DDR DDRC
#define AM2320_PORT PORTC
#define AM2320_PIN PC3


bool AM2320_get_all();
extern int16_t AM2320_temperature;
extern uint16_t AM2320_humidity;

#endif /* AM2330_H_ */