#include "BMP180.h"
#include <avr/io.h>
#include <stdbool.h>
#include <util/delay.h>
#include <util/twi.h>
#include "byte_conversion.h"
#include "twisup.h"
#include "lcdsup.h"

//BMP180 SLA 7-bit 0x77
#define BMP180_SLAW (0xEE)
#define BMP180_SLAR (0xEF)
#define CALIB_ADD (0xAA)
#define MEAS_CTRL_ADD (0xF4)
#define UT_CTRL (0x2E)
#define UP0_CTRL (0x34)
#define ADC_ADD (0xF6)

typedef union{
	uint8_t byte[22];
	struct{
		WORD_TO_BYTE MD;
		WORD_TO_BYTE MC;
		WORD_TO_BYTE MB;
		WORD_TO_BYTE B2;
		WORD_TO_BYTE B1;
		WORD_TO_BYTE AC6;
		WORD_TO_BYTE AC5;
		WORD_TO_BYTE AC4;
		WORD_TO_BYTE AC3;
		WORD_TO_BYTE AC2;
		WORD_TO_BYTE AC1;
	};	
}BMP180_CALIB;

BMP180_CALIB BMP180_calib;
bool BMP180_is_init = false;
uint16_t BMP180_pressure;
uint16_t BMP180_temperature;


/* reg
	-oss 00b [F4h]
		-> 0x2E temp
		-> 0x34 pres0 -- 4.5ms
		-> 0x74 pres1 -- 7.5ms
		-> 0xB4 pres2 -- 13.5ms
		-> 0xF4 pres3 -- 25.5ms
	-id 55h [D0h]
	-calib [AA-BFh]
	-adc out [MSB F6h, LSB F7h, XLSB F8h<7:3>]
	
*/

// bool BMP180_check_id(){
// 	uint8_t id;
// 	twi_controller_start();
// 	twi_controller_send_byte(PT_SLA<<1);
// 	twi_controller_send_byte(0xD0);
// 	twi_controller_start();
// 	twi_controller_send_byte((PT_SLA<<1) | 0x01);
// 	id = twi_controller_get_byte(false);
// 	twi_controller_stop();
// 	
// 	bt_controller_response_buffer[0] = id;
// 	bt_controller_send(1);
// 	
// 	if(id == 0x55) return true;
// 	else return false;
// }

bool BMP180_get_calib(){
	uint32_t i;
	
	if(!twi_controller_start() ||
	   !twi_controller_send_sla(BMP180_SLAW) ||
	   !twi_controller_send_byte(CALIB_ADD) ||
	   !twi_controller_restart() ||
	   !twi_controller_send_sla(BMP180_SLAR))
		return false;
	for(i=0; i<21; i++) BMP180_calib.byte[21-i] = twi_controller_get_byte(true);
	BMP180_calib.byte[0] = twi_controller_get_byte(false);
	twi_controller_stop();
	
	return true;
}

uint8_t times = 0;

bool BMP180_ADC_conv(DWORD_TO_BYTE * result, uint8_t ctrl){
	
	if(!twi_controller_start() ||
 	   !twi_controller_send_sla(BMP180_SLAW) ||
 	   !twi_controller_send_byte(MEAS_CTRL_ADD) ||
	   !twi_controller_send_byte(ctrl))	return false;
	
	if(
	!twi_controller_send_sla(BMP180_SLAW) ||
	!twi_controller_send_byte(MEAS_CTRL_ADD) ||
	!twi_controller_send_byte(ctrl))	return false;
	 
	   
	twi_controller_stop();
	
	_delay_ms(5); //4.5ms
	
	if(!twi_controller_start() ||
	   !twi_controller_send_sla(BMP180_SLAW) ||
	   !twi_controller_send_byte(ADC_ADD) ||
	   !twi_controller_restart() ||
	   !twi_controller_send_sla(BMP180_SLAR)){
		
			return false;
	   }

	result->byte[1] = twi_controller_get_byte(true);
	result->byte[0]= twi_controller_get_byte(false);
	twi_controller_stop();
	
	result->byte[2] = 0;
	result->byte[3] = 0;
	
	return true;	
}

bool BMP180_meas(){
	DWORD_TO_BYTE UT, UP, temperature, pressure;
	int32_t X1, X2, X3, B3, B5, B6;
	uint32_t B4, B7;
	//DEBUG
	PORTD |= (1<<PD7);
	_delay_ms(1);
	
	

	
	
	//#DEBUG 
	//PORTD |= (1<<PD7);
	//_delay_ms(100);
	
	if(!BMP180_ADC_conv(&UT, UT_CTRL) ||
	   !BMP180_ADC_conv(&UP, UP0_CTRL))
		return false;
		
	//calc with ds
	//temp_calc
	X1 = (UT.sdword - BMP180_calib.AC6.word) * BMP180_calib.AC5.word / (1L<<15);
	X2 = BMP180_calib.MC.sword * (1L<<11) / (X1 + BMP180_calib.MD.sword);
	B5 = X1 + X2;
	temperature.sdword = (B5 + 8) / (1<<4);
	
	//press_calc
	B6 = B5 - 4000;
	X1 = (BMP180_calib.B2.sword * (B6*B6 / (1L<<12))) / (1L<<11);
	X2 = BMP180_calib.AC2.sword * B6 / (1L<<11);
	X3 = X1 + X2;
	B3 = ((BMP180_calib.AC1.sword*4 + X3) + 2) / 4;
	X1 = BMP180_calib.AC3.sword * B6 / (1L<<13);
	X2 = (BMP180_calib.B1.sword * (B6*B6 / (1L<<12))) / (1L<<16);
	X3 = ((X1+X2) + 2) / (1<<2);
	B4 = BMP180_calib.AC4.word * (uint32_t)(X3 + 32768) / (1L<<15);
	B7 = (UP.dword-B3) * 50000;
	if(B7 < 0x80000000) pressure.sdword = (B7 * 2) / B4;
	else pressure.sdword = (B7 / B4) * 2;
	X1 = (pressure.sdword / (1L<<8)) * (pressure.sdword / (1L<<8));
	X1 = (X1 * 3038) / (1L<<16);
	X2 = (-7357 * pressure.sdword) / (1L<<16);
	pressure.sdword += (X1 + X2 + 3791) / (1L<<4);
	
	BMP180_temperature = temperature.sdword;
	BMP180_pressure = (pressure.dword)/10;

	//#DEBUG
	_delay_ms(100);
	PORTD &= ~(1<<PD7);
	//PORTD &= ~(1<<PD7);
	_delay_ms(1);
	
	return true;
}