#ifndef BMP180_H_
#define BMP180_H_

#include <avr/io.h>
#include <stdbool.h>
#include "byte_conversion.h"

extern bool BMP180_is_init;
extern uint16_t BMP180_pressure;
extern uint16_t BMP180_temperature;


//bool BMP180_check_id();
bool BMP180_get_calib();
bool BMP180_meas();



#endif /* BMP180_H_ */