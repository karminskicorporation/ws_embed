#include "bt_usart.h"
#include <avr/io.h>
#include <stdbool.h>
#include <util/delay.h>

#define BAUD 38400
//#define U2X_ENABLE

#ifdef U2X_ENABLE
	#define BT_UBRR (F_CPU/8/BAUD-1)
#else
	#define BT_UBRR (F_CPU/16/BAUD-1)
#endif

BT_STATE bt_controller_state = BT_OFF;
bool bt_controller_is_init = false;
uint8_t bt_controller_bytes_to_get = 0;
uint8_t bt_controller_byte_to_send = 0;
char * bt_controller_received_vector = bt_controller_command_buffer;
char * bt_controller_response_vector = bt_controller_response_buffer;
char bt_controller_command_buffer[8];
char bt_controller_response_buffer[128];

void bt_controller_init(){
	if(!(bt_controller_is_init)){
		UBRRH = (uint8_t)(BT_UBRR>>8);
		UBRRL = (uint8_t)BT_UBRR;
		#ifdef U2X_ENABLE
			UCSRA |= (1<<U2X);
		#endif

		UCSRC = (1<<URSEL)|(3<<UCSZ0);
		bt_controller_is_init = true;
	}
	UCSRB = (1<<RXEN)|(1<<TXEN)|(1<<RXCIE);
	bt_controller_state = BT_STANDBY;
}

void bt_controller_disable(){
	UCSRB &= ~((1<<RXEN)|(1<<TXEN)|(1<<RXCIE)|(1<<TXCIE));
	bt_controller_state = BT_OFF;
}

void bt_controller_send(uint8_t bytes_count){
	UDR = bt_controller_response_buffer[0];
	
	if(bytes_count>1){
		bt_controller_byte_to_send = bytes_count-1;
		bt_controller_response_vector = bt_controller_response_buffer+1;
		UCSRB |= (1<<TXCIE);
		bt_controller_state = BT_SENDING;
	}else if(bytes_count==1){
		while (!( UCSRA & (1<<UDRE)));
		//_delay_ms(1); //#TODO why 
		UCSRB |= (1<<RXCIE);
		bt_controller_state = BT_STANDBY;
	}
}

void bt_controller_order(uint8_t bytes_count){
	if(bytes_count>0){
		bt_controller_bytes_to_get = bytes_count;
		bt_controller_received_vector = bt_controller_command_buffer+1;		
		UCSRB |= (1<<RXCIE);
		bt_controller_state = BT_ORDER;
	}else
		bt_controller_state = BT_PROCESSING;
}

// BT_CONTROLLER bt_controller = {
// 	bt_controller_init,
// 	bt_controller_disable,
// 	bt_controller_send,
// 	BT_OFF,
// 	false,
// 	0,
// 	0,
// 	bt_controller.command_buffer,
// 	bt_controller.response_buffer
// };

ISR(USART_RXC_vect){
	uint8_t byte = UDR;

	switch(bt_controller_state){
		case BT_STANDBY:
		bt_controller_command_buffer[0] = byte;
		UCSRB &= ~(1<<RXCIE);
		bt_controller_state = BT_REQUEST;
 		break;
		 
		case BT_ORDER:
		*(bt_controller_received_vector)++ = byte;
		if(--(bt_controller_bytes_to_get)==0){
			UCSRB &= ~(1<<RXCIE);
			bt_controller_state = BT_PROCESSING;
		}
		break;
	}
}

ISR(USART_TXC_vect){
	UDR = *(bt_controller_response_vector)++;
	if(--(bt_controller_byte_to_send)==0){
		UCSRB &= ~(1<<TXCIE);
		UCSRB |= (1<<RXCIE);
		bt_controller_state = BT_STANDBY;
	}
}