#ifndef BT_USART_H_
#define BT_USART_H_
#include <avr/io.h>
#include <stdbool.h>
#include <avr/interrupt.h>

#define BT_DATA_PORTION_SIZE_B 64
#define BT_COEF_SD_SECTOR_BT_PORTION (SIZE_SECTOR/BT_DATA_PORTION_SIZE_B)

#define BT_OK 0xE0
#define BT_NO_MORE_FOR_SEND 0xED
#define BT_PARAM_OUT_OF_RANGE 0xEE
#define BT_PARAM_ERR 0xEF

typedef enum{
	BT_OFF,
	BT_STANDBY,
	BT_REQUEST,
	BT_ORDER,
	BT_PROCESSING,
	BT_SENDING
}BT_STATE;

extern BT_STATE bt_controller_state;
extern bool bt_controller_is_init;
extern char bt_controller_command_buffer[8];
extern char bt_controller_response_buffer[128];

void bt_controller_init();
void bt_controller_disable();
void bt_controller_send(uint8_t bytes_count);
void bt_controller_order(uint8_t bytes_count);

// typedef struct{
// 	void (*init)();
// 	void (*disable)();
// 	void (*send)(uint8_t bytes);
// 	BT_STATE state;
// 	bool is_init;
// 	uint8_t bytes_to_get;
// 	uint8_t byte_to_send;
// 	char * received_vector;
// 	char * response_vector;
// 	char command_buffer[8];
// 	char response_buffer[128];
// }BT_CONTROLLER;

//extern BT_CONTROLLER bt_controller;

#endif /* BT_USART_H_ */