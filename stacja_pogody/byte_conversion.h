#ifndef BYTE_CONVERSION_H_
#define BYTE_CONVERSION_H_

typedef union{
	int32_t sdword;
	uint32_t dword;
	uint8_t byte[4];
}DWORD_TO_BYTE;

typedef union{
	int16_t sword;
	uint16_t word;
	uint8_t byte[2];
}WORD_TO_BYTE;

typedef union{
	uint8_t byte;
	struct{
		uint8_t 
			l_nibble:4,
			h_nibble:4;	
	};
}BYTE_NIBBLE;



#endif /* BYTE_CONVERSION_H_ */