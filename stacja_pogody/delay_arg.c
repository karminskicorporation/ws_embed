#include "delay_arg.h"
#include <util/delay.h>

void delay_ms_arg(uint32_t time){
	if(time<1.0)
		_delay_ms(1);
	else if(time<2)
		_delay_ms(2);
	else if(time<5)
		_delay_ms(5);
	else if(time<10)
		_delay_ms(10);
	else if(time<20)
		_delay_ms(20);
	else if(time<50)
		_delay_ms(50);
	else if(time<100)
		_delay_ms(100);
	else if(time<200)
		_delay_ms(200);
	else if(time<500)
		_delay_ms(500);
	else
		_delay_ms(1000);
}

void delay_us_arg(uint32_t time){
	if(time<1.0)
		_delay_us(1);
	else if(time<2)
		_delay_us(2);
	else if(time<5)
		_delay_us(5);
	else if(time<10)
		_delay_us(10);
	else if(time<20)
		_delay_us(20);
	else if(time<50)
		_delay_us(50);
	else if(time<100)
		_delay_us(100);
	else if(time<200)
		_delay_us(200);
	else if(time<500)
		_delay_us(500);
	else
		#define D_ROUND(x) ((uint32_t)(x)+0.5 > (x) ? (uint32_t)(x) : (uint32_t)(x)+1)
		delay_ms_arg(D_ROUND(time/1000.0));
		#undef D_ROUND
}
