#ifndef DELAY_ARG_H_
#define DELAY_ARG_H_

#include <avr/io.h>

void delay_ms_arg(uint32_t time);
void delay_us_arg(uint32_t time);

#endif /* DELAY_ARG_H_ */