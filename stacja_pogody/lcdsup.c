#include "lcdsup.h"
#include <util/delay.h>
#include <stdbool.h>
#include "byte_conversion.h"

#define LCD_SLA	(0x3F)

#define LCD_RS	(1<<0)
#define LCD_RW	(1<<1)
#define LCD_EN	(1<<2)
#define LCD_BL	(1<<3)

bool alcd_backlight = false;
bool alcd_is_init = false;
char alcd_line[2][16];


/*support function*/ 


void lcd_4bit_send(bool is_comm, uint8_t byte){
	uint8_t options = (is_comm ? 0x00 : LCD_RS) | (alcd_backlight ? LCD_BL : 0x00);
	//BYTE_NIBBLE b2n; //#TODO
	//b2n.byte = byte;
	twi_controller_start();
	twi_controller_send_byte(LCD_SLA<<1);
	twi_controller_send_byte((/*b2n.h_nibble*/byte&0xF0)<<0|options|LCD_EN);
	twi_controller_send_byte((/*b2n.h_nibble*/byte&0xF0)<<0|options);
	twi_controller_send_byte((/*b2n.l_nibble*/byte&0x0F)<<4|options|LCD_EN);
	twi_controller_send_byte((/*b2n.l_nibble*/byte&0x0F)<<4|options);
	twi_controller_stop();
}	

void lcd_8bit_comm_send(/*bool is_comm,*/ uint8_t byte){
	twi_controller_start();
	twi_controller_send_byte(LCD_SLA<<1);
	twi_controller_send_byte(byte|LCD_EN);
	twi_controller_send_byte(byte);
	twi_controller_stop();
}

/*
uint8_t lcd_4bit_receive(bool is_comm){
	uint8_t options = (is_comm ? 0x00 : LCD_RS) | (alcd.backlight ? LCD_BL : 0x00);
	uint8_t resp[2];
	twi_controller.start();
	twi_controller.send_byte((LCD_SLA<<1)|LCD_RW);
	resp[0] = twi_controller.get_byte(true);
	resp[1] = twi_controller.get_byte(false);
	twi_controller.stop();
	return (((resp[0])&0xF0)<<4)|((resp[1])&0xF0);
}
*/

/*advanved*/

void alcd_init(){
	_delay_ms(40);
	//change 8-bit to 4-bit transfer
// 	twi_controller_start();
// 	twi_controller_send_byte(LCD_SLA<<1);
// 	twi_controller_send_byte(0x30|LCD_EN);
// 	twi_controller_send_byte(0x30);
// 	twi_controller_stop();
	lcd_8bit_comm_send(0x30);
	_delay_ms(5);
	
// 	twi_controller_start();
// 	twi_controller_send_byte(LCD_SLA<<1);
// 	twi_controller_send_byte(0x20|LCD_EN);
// 	twi_controller_send_byte(0x20);
// 	twi_controller_stop();
	lcd_8bit_comm_send(0x20);
	_delay_us(200);

// 	twi_controller_start();
// 	twi_controller_send_byte(LCD_SLA<<1);
// 	twi_controller_send_byte(0x20|LCD_EN);
// 	twi_controller_send_byte(0x20);
// 	twi_controller_stop();
	lcd_8bit_comm_send(0x20);
	_delay_us(50);
	
	//init
	lcd_4bit_send(true, (1<<5)|(1<<3)); //4-bit, 2 lines, small font
	_delay_us(50);
	lcd_4bit_send(true, (1<<2)|(1<<1)); //increment, no shift
	_delay_us(50);
	lcd_4bit_send(true, (1<<3)|(1<<2)|(1<<1)); //on display, cursor, no blinking
	_delay_us(50);
	lcd_4bit_send(true, (1<<0)); //clear display
	_delay_ms(100);
	alcd_is_init = true;
}

void alcd_print(bool line, uint8_t position){
	uint8_t i;
	lcd_4bit_send(true, (1<<7)|(position+line*0x40));
	_delay_us(50);
	//for(i=0; alcd_line[line][i]!='\0' && i<0x27; i++){
	for(i=0; i<16; i++){
		lcd_4bit_send(false, alcd_line[line][i]);
		_delay_us(50);
	}	
	for(i=0;i<16;++i){
		alcd_line[0][i] = ' ';
		alcd_line[1][i] = ' ';
	}
}

void alcd_switch_backlight(bool is_on){
	if(!is_on){
		alcd_backlight = false;
		if(alcd_is_init){
			lcd_4bit_send(true, (1<<3)); //off display, no cursor, no blinking
			_delay_us(50);
		}else{
			twi_controller_start();
			twi_controller_send_byte(LCD_SLA<<1);
			twi_controller_send_byte(0x00);
			twi_controller_stop();
		}
	}else{
		alcd_backlight = true;
		if(alcd_is_init){
			lcd_4bit_send(true, (1<<3)|(1<<2)|(1<<1)); //on display, cursor, no blinking
			_delay_us(50);
		}
		else{
			twi_controller_start();
			twi_controller_send_byte(LCD_SLA<<1);
			twi_controller_send_byte(LCD_BL);
			twi_controller_stop();
		}
	}
}

void alcd_str2line(bool line, char* str){
	uint8_t i;
	for(i=0; i<16; i++)
		alcd_line[line][i] = str[i];
// 	for(i=0; str[i]!='\0'; i++)
// 		alcd_line[line][i] = str[i];
// 	for( ; i<16; i++)
// 		alcd_line[line][i] = ' ';
// 	alcd_line[line][16] = '\0';
}

void alcd_hex2line(bool line, uint8_t offset, uint8_t num){
	char sign[2];
	sign[1] = (num&0xF0)>>4;
	sign[0] = num&0x0F;
	alcd_line[line][0+offset] = (sign[1]<0xA ? '0'+sign[1] : 'A'+(sign[1]-10));
	alcd_line[line][1+offset] = (sign[0]<0xA ? '0'+sign[0] : 'A'+(sign[0]-10));
}

// ADVANCED_LCD alcd = {
// 	alcd_init,
// 	alcd_print,
// 	alcd_switch_backlight,
// 	alcd_str2line,
// 	false,
// 	false
// };


