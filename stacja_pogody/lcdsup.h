#ifndef LCD_H_
#define LCD_H_
#include "twisup.h"
#include <avr/io.h>
#include <stdbool.h>

/*extern bool alcd_backlight;*/
extern bool alcd_is_init;
extern char alcd_line[2][16];

void alcd_init();
void alcd_print(bool line, uint8_t position);
void alcd_switch_backlight(bool is_on);
void alcd_str2line(bool line, char* str);
void alcd_hex2line(bool line, uint8_t offset, uint8_t num);
// typedef struct{
// 	void (*init)();
// 	void (*print)(bool line, uint8_t position);
// 	void (*switch_backlight)(bool is_on);
// 	void (*str2line)(bool line, char* str);
// 	bool backlight;
// 	bool is_init;
// 	char line[2][16];
// }ADVANCED_LCD;

//extern ADVANCED_LCD alcd;

#endif /* LCD_H_ */