#include <avr/io.h>
#include <avr/interrupt.h> 
#include "power_supply.h"
#include "avr/sleep.h"
#include "twisup.h"

#define BATT_CHANNEL (0);
#define MAINS_CHANNEL (1);

POWER_TYPE ps_controller_power_type = POWER_BATTERY;
BATTERY_STATE ps_controller_battery_state = BATTERY_OK;
uint16_t ps_controller_r_batt = 0;
uint16_t ps_controller_r_mains = 0;
uint8_t ps_controller_secs_counter = 0;
uint8_t ps_controller_secs_to_off = 0;
uint8_t ps_controller_secs_to_reon = 0;
float ps_controller_batt_prescaler = (10.0+4*2.2)/(4*2.2);
float ps_controller_mains_prescaler = (10.0+2*2.2+3*1.0)/(2*2.2+3*1.0);

void ps_controller_init(){
	ADCSRA = (1<<ADEN) | (1<<ADPS1) | (1<<ADPS0) | (1<<ADIE);
	ADMUX = (1<<REFS1) | (1<<REFS0);
	//pomiar inicjalizacyjny
	ADCSRA |= (1<<ADSC);
	while(ADCSRA & (1<<ADSC));
}

void ps_controller_disable(){
	ADCSRA &= ~((1<<ADEN) | (1<<ADIE));
}

void ps_controller_get_voltage(){
	twi_controller_off(); //before sleep
	
	ADMUX &= ~(0xF);
	ADMUX |= BATT_CHANNEL;
	set_sleep_mode (SLEEP_MODE_ADC);
	sleep_enable();
	sleep_cpu();
	sleep_disable();
	ps_controller_r_batt = ADC;
	
	ADMUX &= ~(0xF);
	ADMUX |= MAINS_CHANNEL;
	set_sleep_mode (SLEEP_MODE_ADC);
	sleep_enable();
	sleep_cpu();
	sleep_disable();
	ps_controller_r_mains = ADC;
}

EMPTY_INTERRUPT(ADC_vect);


// uint16_t ps_controller_get_batt_mv(){
// 	return (uint16_t)(2.56*ps_controller_batt_prescaler*ps_controller_r_batt*1000/1024);
// }
// 
// uint16_t ps_controller_get_mains_mv(){
// 	return (uint16_t)(2.56*ps_controller_mains_prescaler*ps_controller_r_mains*1000/1024);
// }


void ps_controller_set_state(){
	
	//4.7V == 800 (17.4/7.4)
	if(ps_controller_r_mains<800 && false) //#TODO #DEBUG
		ps_controller_power_type = POWER_BATTERY;	
	else
		ps_controller_power_type = POWER_MAIN;
	
		
	//3.4V == 637 (18.8/8.8)
	if(ps_controller_r_batt<637)
		ps_controller_battery_state = BATTERY_CRITICAL;	
	//3.65V == 684 (18.8/8.8)
	else if(ps_controller_r_batt<684)
		ps_controller_battery_state = BATTERY_LOW;
	//5.35V == 1002 (18.8/8.8)
	else if(ps_controller_r_batt<1002)
		ps_controller_battery_state = BATTERY_OK;
	else
		ps_controller_battery_state = BATTERY_TO_HIGH;
	
}


void ps_controller_init_blinking(uint8_t secs_on, uint8_t secs_off){
	ps_controller_secs_to_off = secs_on;
	ps_controller_secs_to_reon = secs_on+secs_off;
}

void ps_controller_blink_sec(uint8_t port, uint8_t pin){
	if(ps_controller_secs_counter>=ps_controller_secs_to_reon)
		ps_controller_secs_counter %= ps_controller_secs_to_reon;
	if(ps_controller_secs_counter==0)
		//port |= (1<<pin);
		PORTD |= (1<<PD5);
	else if(ps_controller_secs_counter==ps_controller_secs_to_off)
		//port &= ~(1<<pin);
		PORTD &= ~(1<<PD5);	
}


void ps_controller_set_sleep(){
	set_sleep_mode(SLEEP_MODE_IDLE);
	sleep_enable();
	sleep_cpu();
	sleep_disable();
}

// PS_CONTROLLER ps_controller = {
// 	ps_controller_init,
// 	ps_controller_disable,
// 	ps_controller_get_voltage,
// 	//ps_controller_get_batt_mv,
// 	//ps_controller_get_mains_mv,
// 	ps_controller_set_state,
// 	//ps_controller_init_blinking,
// 	//ps_controller_blink_sec,
// 	ps_controller_set_sleep,
// 	//(10.0+4*2.2)/(4*2.2), //batt zolty
// 	//(10.0+2*2.2+3*1.0)/(2*2.2+3*1.0), //mains zielony
// 	POWER_BATTERY,
// 	BATTERY_OK,
// 	0
// };
