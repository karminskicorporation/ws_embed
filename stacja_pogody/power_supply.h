#ifndef POWER_SUPPLY_H_
#define POWER_SUPPLY_H_

#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdbool.h>

typedef enum{
	POWER_MAIN, //4.7V
	POWER_BATTERY
}POWER_TYPE;

typedef enum{
	BATTERY_TO_HIGH,
	BATTERY_OK, //5.35V
	BATTERY_LOW, //3.65V
	BATTERY_CRITICAL //3.4V
}BATTERY_STATE;

extern POWER_TYPE ps_controller_power_type;
extern BATTERY_STATE ps_controller_battery_state;
extern uint16_t ps_controller_r_batt;
extern uint16_t ps_controller_r_mains;
extern uint8_t ps_controller_secs_counter;
extern uint8_t ps_controller_secs_to_off;
extern uint8_t ps_controller_secs_to_reon;
extern float ps_controller_batt_prescaler;
extern float ps_controller_mains_prescaler;

void ps_controller_init();
void ps_controller_disable();
void ps_controller_get_voltage();
void ps_controller_set_state();
void ps_controller_set_sleep();
void ps_controller_init_blinking(uint8_t secs_on, uint8_t secs_off);
void ps_controller_blink_sec(uint8_t port, uint8_t pin);

// typedef struct{
// 	void (*init)();
// 	void (*disable)();
// 	void (*get_voltage)();
// 	//uint16_t (*get_batt_mv)();
// 	//uint16_t (*get_mains_mv)();
// 	void (*set_state)();
// 	//void (*init_blinking)(uint8_t secs_on, uint8_t secs_off);
// 	//void (*blink_sec)(uint8_t port, uint8_t pin);
// 	void (*set_sleep)();
// 	//float batt_prescaler;
// 	//float mains_prescaler;
// 	POWER_TYPE power_type;
// 	BATTERY_STATE battery_state;
// 	uint8_t secs_counter;
// 	uint16_t r_batt;
// 	uint16_t r_mains;
// 	uint8_t secs_to_off;
// 	uint8_t secs_to_reon;
// }PS_CONTROLLER;

//extern PS_CONTROLLER ps_controller;

#endif /* POWER_SUPPLY_H_ */