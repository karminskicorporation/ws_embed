#include "refresh_control.h"
#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdbool.h>

#define T1_PRESC 1024

#if F_CPU == 8000000UL && T1_PRESC == 1024UL
	//(65536UL*1000*1000/F_CPU*T1_PRESC)
	#define T1_OVF_US 8388608UL
	//(1000UL*1000*T1_PRESC/F_CPU)
	#define T1_RES_US 128UL
#else
	#error "Nieobslugiwana czestotliwosc lub preskaler!"
#endif

#define T1_COMPB_SHIFT_MS 100UL

bool refresh_control_refresh_lcd_flag = false;
bool refresh_control_take_meas_flag = false;
uint16_t refresh_control_refresh_lcd_shiftA = 0;
uint16_t refresh_control_take_meas_shiftB = 0;
uint32_t refresh_control_meas_period_ms = 0;
uint32_t refresh_control_meas_match_ms = 0;

void refresh_control_init(uint32_t lcd_refresh_period_ms, uint32_t meas_period_s){
	#if T1_PRESC == 1024UL
		TCCR1B |= (1<<CS12) | (1<<CS10);
	#else
		#error "Niezgodny preskaler!"
	#endif
	refresh_control_refresh_lcd_shiftA = lcd_refresh_period_ms*1000/T1_RES_US;
	OCR1A = refresh_control_refresh_lcd_shiftA;
	refresh_control_take_meas_shiftB = 1000UL*T1_COMPB_SHIFT_MS/T1_RES_US;
	OCR1B = refresh_control_take_meas_shiftB;
	refresh_control_meas_match_ms = meas_period_s*1000;
	TIMSK |= (1<<OCIE1A) | (1<<OCIE1B);
	refresh_control_refresh_lcd_flag = false;
	refresh_control_take_meas_flag = false;
}

void refresh_control_disable(){
	TIMSK &= ~((1<<OCIE1A) | (1<<OCIE1B) | (1<<TOIE1));
	TCCR1B &= ~((1<<CS12) | (1<<CS11) | (1<<CS10));
}

// REFRESH_CONTROL refresh_control = {
// 	refresh_control_init,
// 	refresh_control_disable,
// 	false,
// 	false,
// 	0,
// 	0,
// 	0,
// 	0
// };

ISR(TIMER1_COMPA_vect){ //lcd refresh
	refresh_control_refresh_lcd_flag = true;
	OCR1A += refresh_control_refresh_lcd_shiftA;
}

ISR(TIMER1_COMPB_vect){ //meas take
	OCR1B += refresh_control_take_meas_shiftB;
	refresh_control_meas_period_ms += T1_COMPB_SHIFT_MS;
	if(refresh_control_meas_period_ms>refresh_control_meas_match_ms){
		refresh_control_meas_period_ms -= refresh_control_meas_match_ms;
		refresh_control_take_meas_flag = true;
	}
}