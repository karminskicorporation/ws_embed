#ifndef REFRESH_CONTROL_H_
#define REFRESH_CONTROL_H_

#include <avr/io.h>
#include <stdbool.h>

extern bool refresh_control_refresh_lcd_flag;
extern bool refresh_control_take_meas_flag;
extern uint16_t refresh_control_refresh_lcd_shiftA;
extern uint16_t refresh_control_take_meas_shiftB;
extern uint32_t refresh_control_meas_period_ms;
extern uint32_t refresh_control_meas_match_ms;

void refresh_control_init(uint32_t lcd_refresh_period_ms, uint32_t meas_period_s);
void refresh_control_disable();
// typedef struct{
// 	void (*init)(uint32_t lcd_refresh_period_ms, uint32_t meas_period_s);
// 	void (*disable)();
// 	bool refresh_lcd_flag;
// 	bool take_meas_flag;
// 	uint16_t refresh_lcd_shiftA;
// 	uint16_t take_meas_shiftB;
// 	uint32_t meas_period_ms;
// 	uint32_t meas_match_ms;
// }REFRESH_CONTROL;

//extern REFRESH_CONTROL refresh_control;


#endif /* REFRESH_CONTROL_H_ */