#include "rtc_meas.h"
//#include "bt_usart.h"

// #define T2_PRESC 1024
// #define RTC_TSTAMP (1471616280)
// 
// #if F_CPU == 8000000 && T2_PRESC == 1024
// 	//(256UL*1000*1000/F_CPU*T2_PRESC)
// 	#define T2_OVF_US 32768
// 	//(1000UL*1000*T2_PRESC/F_CPU)
// 	#define T2_RES_US 128
// #else
// 	#error "Nieobslugiwana czestotliwosc lub preskaler!"
// #endif


bool rtc_meas_is_reset = true;
bool rtc_meas_sec_flag = false;
bool rtc_meas_check_voltage_flag = false;
uint32_t rtc_meas_timestamp = 0;
//uint32_t rtc_meas_timestamp_us = 0;
uint8_t rtc_meas_match_shift = 0;


bool rtc_meas_init(uint16_t voltage_match_ms){
// 	#if T2_PRESC == 1024
// 		TCCR2 = (1<<CS22) | (1<<CS21) | (1<<CS20);
// 	#else
// 		#error "Niezgodny preskaler!"
// 	#endif

	if(voltage_match_ms<1000/256 || voltage_match_ms>1000) return false;
	rtc_meas_match_shift = 256UL*voltage_match_ms/1000;

	ASSR |= (1<<AS2);
	OCR2 = rtc_meas_match_shift;
	TCCR2 = (1<<CS22) | (1<<CS20); //prsclaer 128
	while(ASSR & ((1<<TCN2UB) | (1<<OCR2UB) | (1<<TCR2UB))); //until flag is not 0
	TIFR |= (1<<OCF2) | (1<<TOV2);
	
	TIMSK |= (1<<OCIE2) | (1<<TOIE2);
	return true;
}




uint16_t rtc_meas_get_ms(){
	//return 1UL*TCNT2*T2_RES_US/1000;
	return 1000UL*TCNT2/256; //256 == 1s
}

// bool rtc_meas_init_match(uint16_t ms){
// // 	if(1000UL*ms>T2_OVF_US || 1000UL*ms<T2_RES_US) return false;
// // 	rtc_meas_match_shift = 1000UL*ms/T2_RES_US;
// // 	OCR2 = rtc_meas_match_shift;
// // 	TIMSK |= (1<<OCIE2);
// // 	return true;
// 	if(ms<1000/256 || ms>1000) return false;
// 	rtc_meas_match_shift = 256UL*ms/1000;
// 	OCR2 = rtc_meas_match_shift;
// 	TIMSK |= (1<<OCIE2);
// 	return true;
// }

// RTC_MEAS rtc_meas = {
// 	rtc_meas_init,
// 	rtc_meas_init_match,
// 	rtc_meas_get_ms,
// 	true,
// 	false,
// 	false,
// 	RTC_TSTAMP,
// 	0,
// 	0xFF
// };

ISR(TIMER2_OVF_vect){
	//rtc_meas_timestamp_us += T2_OVF_US;
	//if(rtc_meas_timestamp_us>1000UL*1000){
	++(rtc_meas_timestamp);
		//rtc_meas_timestamp_us -= 1000UL*1000;
	rtc_meas_sec_flag=true;
	//}
}

ISR(TIMER2_COMP_vect){
	OCR2 += rtc_meas_match_shift;
	rtc_meas_check_voltage_flag = true;
}