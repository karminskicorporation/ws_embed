#ifndef RTC_MEAS_H_
#define RTC_MEAS_H_

#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdbool.h>

extern bool rtc_meas_is_reset;
extern bool rtc_meas_sec_flag;
extern bool rtc_meas_check_voltage_flag;
extern uint32_t rtc_meas_timestamp;
//extern uint32_t rtc_meas_timestamp_us;
extern uint8_t rtc_meas_match_shift;

bool rtc_meas_init(uint16_t voltage_match_ms);
//bool rtc_meas_init_match(uint16_t ms);
uint16_t rtc_meas_get_ms();

// typedef struct{
// 	void (*init)();
// 	bool (*init_match)(uint16_t ms);
// 	uint16_t (*get_ms)();
// 	bool is_reset;
// 	bool sec_flag;
// 	bool check_voltage_flag;
// 	uint32_t timestamp;
// 	uint32_t timestamp_us;
// 	uint8_t match_shift;
// }RTC_MEAS;

//extern RTC_MEAS rtc_meas;

#endif /* RTC_MEAS_H_ */