#include "sd_storage.h"
#include <stdbool.h>
#include <util/delay.h>
#include <avr/io.h>



bool sd_model_is_overflow = false;
SD_INFO sd_model_info;
uint8_t sd_model_buffer[SIZE_SECTOR];

//# SPI SUPPORT #//
#define SPI_EIGHT_CLOCK() spi_write(0xFF)
#define SPI_GET_BYTE() spi_write(0xFF)

void spi_init(){
	CS_DDR |= CS; // SD card circuit select as output
	DDRB |= MOSI | SCK; // MOSI and SCK as outputs
	//PORTB |= MISO; // pullup in MISO, might not be needed
	//#TODO until 5.0 to 3v3 transformer
	
	// Enable SPI, master, set clock rate fclk/4 = 125kHz (double speed)
	SPCR = (1<<SPE) | (1<<MSTR) | (1<<SPR1) | (1<<SPR0);
	SPSR |= (1<<SPI2X);
}

static uint8_t spi_write(uint8_t byte) {
	SPDR = byte;
	while(!(SPSR & (1<<SPIF)));
	return SPDR;
}

//# SD SUPPORT -- LOW LEVEL #//

//comment if don't care
//#define CHECK_SD_SUPPORT_VOLTAGE_RANGE

#define CMD0 0x40
#define CMD8 0x48
#define CMD58 0x7A
#define CMD55 0x77
#define ACMD41 0x69
#define CMD16 0x50
#define CMD24 0x58
#define CMD13 0x4D
#define CMD17 0x51
#define CMD9 0x49

#define START_TOKEN 0xFE

static void sd_command(uint8_t cmd, uint32_t arg, uint8_t crc){
	spi_write(0xFF);
	spi_write(cmd);
	spi_write(arg>>24);
	spi_write(arg>>16);
	spi_write(arg>>8);
	spi_write(arg);
	spi_write(crc);
	spi_write(0xFF);
}

SD_STATUS_COMM sd_init(){
	uint8_t i;
	uint32_t k;
	uint32_t block_length = 512;
	uint32_t c_size;
	uint32_t mult = 512;
	SD_R1 r1;
	SD_R7 r7;
	SD_OCR ocr;
	SD_CSD_v10 csd_v10;
	SD_CSD_v20 csd_v20;
	
	//incjalizacja zasilania
	//#TODO podlaczenie pinu pod zasilanie?
	CS_DISABLE();
	for(i=0; i<10; i++) SPI_EIGHT_CLOCK(); //74 clocks wait
	CS_ENABLE();  
	for(i=0; i<3; i++) SPI_EIGHT_CLOCK(); //16 clocks wait
	
	//CMD0 reset & sdbus to SPI change
	sd_command(CMD0, 0x00000000, 0x95);
	r1.byte = SPI_GET_BYTE();
	if(r1.byte != IN_IDLE_STATE) return SD_TO_SPI_SWITCH_ERR;
	
	//CMD8 VHS check and version recognize
	sd_command(CMD8, 0x000001AA, 0x87);	
	r7.byte[4] = SPI_GET_BYTE();
	if(r7.r1.byte == IN_IDLE_STATE){ //v2.0
		sd_model_info.version = SD_gt_v20;
		for(i=0; i<4; i++) r7.byte[3-i] = SPI_GET_BYTE();
		if(r7.other!=0x000001AA) return SD_VHS_GENERAL_ERR; 
	}else if((r7.r1.byte)&ILLEGAL_COMMAND) //v1.x or not compatible
		sd_model_info.version = SD_v1x;
	else return SD_UNEXPECTED_ERR;
		 
#ifdef CHECK_SD_SUPPORT_VOLTAGE_RANGE
	//CMD58 get voltage range
	sd_command(CMD58, 0x00000000, 0x00);
	r1.byte = SPI_GET_BYTE();
	if(r1.byte != IN_IDLE_STATE) return SD_UNEXPECTED_ERR;
	else{
		for(i=0; i<4; i++) ocr.byte[3-i] = SPI_GET_BYTE();
		if(!(ocr.v31_32 && ocr.v32_33 && ocr.v33_34))
			return SD_VHS_DETAILED_ERR;
	}
#endif
	
	//ACMD41 leaving idle state
	for(k=0; k<1000; k++){
		sd_command(CMD55, 0x00000000, 0x00);
		r1.byte = SPI_GET_BYTE();
		if((r1.byte)&ILLEGAL_COMMAND && sd_model_info.version == SD_v1x)
			return SD_NOT_SUPPORTED_CARD;
		else if(r1.byte != IN_IDLE_STATE) return SD_UNEXPECTED_ERR;
			
		sd_command(ACMD41, (sd_model_info.version == SD_gt_v20 ? 0x40000000 : 0x00000000), 0x00);
		r1.byte = SPI_GET_BYTE();
		if(r1.byte == IN_IDLE_STATE) continue;
		else if(r1.byte == ACTIVE_STATE) break;
		else return SD_UNEXPECTED_ERR;
	}
	if(k==1000) return SD_OUT_OF_IDLE_ERR;  //#TODO to timeout change ~60ms
		
	if(sd_model_info.version == SD_v1x)
		sd_model_info.type = SDSC;
	else{
		//CMD58 get CCS to recognize sdsc or sdhc
		sd_command(CMD58, 0x00000000, 0x00);
		r1.byte = SPI_GET_BYTE();
		if(r1.byte == ACTIVE_STATE){
			for(i=0; i<4; i++) ocr.byte[3-i] = SPI_GET_BYTE();
			if(ocr.ccs) sd_model_info.type = SDHC;
			else sd_model_info.type = SDSC;
		}else return SD_UNEXPECTED_ERR;	
	}
	
	//CMD9 get capacity card
	sd_command(CMD9, 0x00000000, 0x00);
	for(i=0; i<8; i++){ //wait for r1
		r1.byte = SPI_GET_BYTE();
		if(r1.byte == ACTIVE_STATE) break;
	}
	if(i==8) return SD_UNEXPECTED_ERR;
	for(i=0; i<8; i++){ //wait for token
		if(SPI_GET_BYTE() == START_TOKEN) break;
	}
	if(i==8) return SD_UNEXPECTED_ERR;
	//reading CSD
	if(sd_model_info.type == SDSC){
		for(i=0; i<16; i++) csd_v10.byte[15-i] = SPI_GET_BYTE();
		SPI_GET_BYTE();
		SPI_GET_BYTE(); //crc
		c_size = C_SIZE_v10(csd_v10)+1;
		mult = 1<<(C_SIZE_MULT_v10(csd_v10)+2);
		block_length = 1 << csd_v10.read_bl_len;
		sd_model_info.sectors_cap = block_length/SIZE_SECTOR*mult*c_size;
	}else if(sd_model_info.type == SDHC){
		for(i=0; i<16; i++) csd_v20.byte[15-i] = SPI_GET_BYTE();
		SPI_GET_BYTE();
		SPI_GET_BYTE(); //crc
		c_size = C_SIZE_v20(csd_v20)+1;
		sd_model_info.sectors_cap = 1024*c_size;
	}
	if(sd_model_info.type == SDSC){
		//CMD16 set 512 block size
		sd_command(CMD16, SIZE_SECTOR, 0x00);
		r1.byte = SPI_GET_BYTE();
		if(r1.byte != ACTIVE_STATE) return SD_UNEXPECTED_ERR;
	}
	return SD_OK;
}

uint8_t response[16];
uint8_t counter_wrt = 0x00;
uint8_t control;

bool sd_model_write_sector(uint32_t sector, uint8_t * data, uint16_t start_pos, uint16_t bytes_amount){	
	uint32_t i;
	SD_R1 r1;
	SD_R2 r2;
	
	//CMD24 write data
	sd_command(CMD24, (sd_model_info.type == SDSC ? sector*512 : sector), 0x00);
	r1.byte = SPI_GET_BYTE();
	if(r1.byte != ACTIVE_STATE){
		sd_model_info.status = SD_WRITE_ERR;
		return false; //SD_UNEXPECTED_ERR;
	}
	
	spi_write(START_TOKEN);	
	for(i=0; i<start_pos; i++) spi_write(SD_BLANK_BYTE); //skip before
	for(i=start_pos; i<start_pos+bytes_amount; i++) spi_write(*data++);
	for(i=start_pos+bytes_amount; i<SIZE_SECTOR; i++) spi_write(SD_BLANK_BYTE);//skip after
	
	SPI_GET_BYTE();
	SPI_GET_BYTE(); //crc
	
	control = SPI_GET_BYTE();
	
	for(i=0; i<64; i++){
		_delay_ms(1);
		if(SPI_GET_BYTE() == 0xFF) break;
	}
	if(i==64) return false;//SD_UNEXPECTED_ERR;
		
	if(((control)&0x1F) == 0x05){
		//CMD13 check status
		sd_command(CMD13, 0x00000000, 0x00);
		r2.byte[1] = SPI_GET_BYTE();
		r2.byte[0] = SPI_GET_BYTE();
		if(r2.all != 0x0000){
			sd_model_info.status = SD_WRITE_ERR;
			return false; //SD_UNEXPECTED_ERR;
		}
	}
	else return false;//SD_UNEXPECTED_ERR;
	return true;//SD_OK;
}

bool sd_model_read_sector(uint32_t sector, uint8_t * data, uint16_t start_pos, uint16_t bytes_amount){
	uint32_t i;
	SD_R1 r1;
	//CMD17 read block
	sd_command(CMD17, (sd_model_info.type == SDSC ? sector*512 : sector), 0x00);
	for(i=0; i<8; i++){
		r1.byte = SPI_GET_BYTE();
		if(r1.byte == ACTIVE_STATE) break;
	}
	if(i==8){
		sd_model_info.status = SD_READ_ERR;
		return false;//SD_UNEXPECTED_ERR;
	}

	for(i=0; i<8; i++) if(SPI_GET_BYTE() == START_TOKEN) break;
	if(i==8){
		sd_model_info.status = SD_READ_ERR;
		return false; //SD_UNEXPECTED_ERR;
	}
	
	
	for(i=0; i<start_pos; i++) spi_write(0xFF); //skip before
	for(i=start_pos; i<start_pos+bytes_amount; i++) *data++ = spi_write(0xFF);	
	for(i=start_pos+bytes_amount; i<SIZE_SECTOR; i++) spi_write(0xFF);//skip after
	SPI_GET_BYTE();
	SPI_GET_BYTE(); //crc from card
	return true;//SD_OK;
}

//# SD SUPPORT -- HIGH LEVEL #//

bool sd_model_state_recognize(){
	SD_MAIN_SECTOR  main_sector;
	
	//# SD INIT #//
	
	switch(sd_init()){
		case SD_OK:
		if(sd_model_info.status != SD_INIT)
		sd_model_info.status = SD_UNKNOWN;
		break;
		
		case SD_TO_SPI_SWITCH_ERR:
		sd_model_info.status = SD_ABSENT;
		return false;
		break;
		
		case SD_VHS_GENERAL_ERR:
		case SD_VHS_DETAILED_ERR:
		case SD_OUT_OF_IDLE_ERR:
		case SD_NOT_SUPPORTED_CARD:
		sd_model_info.status = SD_UNSUPPORTED;
		return false;
		break;
		
		case SD_UNEXPECTED_ERR:
		default:
		sd_model_info.status = SD_FAILED;
		return false;
		break;		
	}
	
	//# MAIN SECTOR #//
	
	if(!(sd_model_read_sector(0, main_sector.byte, 0, 8))){
		sd_model_info.status = SD_FAILED;
		return false;
	}
		
	//# CHECK MAIN SECTOR WITH SRAM #//
	
	//main_sector = (SD_MAIN_SECTOR *)(sd_model_buffer);
	
	if(main_sector.id != SD_ID){ 
		sd_model_info.status = SD_NOT_INIT;
		return false;
	}
	
	if(sd_model_info.status != SD_UNKNOWN &&
	   main_sector.current_sector != sd_model_info.current_sector){
		sd_model_info.status = SD_OUT_OF_SYNC;
		return false;   
	}
	
	//# GET DATA TO SRAM TO FIRST SYNC #//
	if(sd_model_info.status == SD_UNKNOWN){
		sd_model_info.sectors_occp = main_sector.current_sector-1;
		sd_model_info.current_sector = main_sector.current_sector;
	}
	
	sd_model_info.status = SD_INIT;
	return true;
}

bool sd_model_write_main_sector(SD_MAIN_SECTOR main_sector){
	if(!(sd_model_write_sector(0, main_sector.byte, 0, 8))){
		sd_model_info.status = SD_MAIN_SECTOR_UPDATE_ERR;
		return false;
	}
	return true;
}

bool sd_model_clear_main_sector(){
	SD_MAIN_SECTOR main_sector;
	main_sector.id = SD_ID;
	main_sector.current_sector = 1;
	if(!(sd_model_write_main_sector(main_sector))) return false;
	sd_model_info.current_sector = 1;
	sd_model_info.sectors_occp = 0;
	return true;
}

//#include "bt_usart.h"

bool sd_model_add_sector_data(uint8_t * data){
	SD_MAIN_SECTOR main_sector;
	
	//#DEBUG
// 	uint32_t i;
// 	for(i=0; i<128; i++) bt_controller_response_buffer[i] = sd_model_buffer[i];
// 	bt_controller_send(128);
// 	_delay_ms(100);
// 	for(i=0; i<128; i++) bt_controller_response_buffer[i] = sd_model_buffer[i+128];
// 	bt_controller_send(128);
// 	_delay_ms(100);
// 	for(i=0; i<128; i++) bt_controller_response_buffer[i] = sd_model_buffer[i+128*2];
// 	bt_controller_send(128);
// 	_delay_ms(100);
// 	for(i=0; i<128; i++) bt_controller_response_buffer[i] = sd_model_buffer[i+128*3];
// 	bt_controller_send(128);
// 	_delay_ms(100);
	
	if(!(sd_model_state_recognize())) return false;
	if(!(sd_model_write_sector(sd_model_info.current_sector, sd_model_buffer, 0, SIZE_SECTOR))){
		sd_model_info.status = SD_WRITE_ERR;
		return false;
	}
	++(sd_model_info.current_sector);
	++(sd_model_info.sectors_occp);

	main_sector.id = SD_ID;
	main_sector.current_sector = sd_model_info.current_sector;
	if(!(sd_model_write_main_sector(main_sector))) return false;
	
// 	#DEBUG
// 		if(sd_model_read_sector(sd_model_info.current_sector-1, bt_controller_response_buffer, 0, 128)){
// 			bt_controller_send(128);
// 			_delay_ms(100);
// 		}else{
// 			bt_controller_response_buffer[0] = SD_FAILED;
// 			bt_controller_send(1);
// 			return false;
// 		}
// 		//for(i=0; i<128; i++) bt_controller_response_buffer[i] = 0;
// 		if(sd_model_read_sector(sd_model_info.current_sector-1, bt_controller_response_buffer, 128, 128)){
// 			bt_controller_send(128);
// 			_delay_ms(100);
// 		}else{
// 			bt_controller_response_buffer[0] = SD_FAILED;
// 			bt_controller_send(1);
// 			return false;
// 		}
// 		//for(i=0; i<128; i++) bt_controller_response_buffer[i] = 0;
// 		if(sd_model_read_sector(sd_model_info.current_sector-1, bt_controller_response_buffer, 128*2, 128)){
// 			bt_controller_send(128);
// 			_delay_ms(100);
// 		}else{
// 			bt_controller_response_buffer[0] = SD_FAILED;
// 			bt_controller_send(1);
// 			return false;
// 		}
// 		//for(i=0; i<128; i++) bt_controller_response_buffer[i] = 0;
// 		if(sd_model_read_sector(sd_model_info.current_sector-1, bt_controller_response_buffer, 128*3, 128)){
// 			bt_controller_send(128);
// 			_delay_ms(100);
// 		}else{
// 			bt_controller_response_buffer[0] = SD_FAILED;
// 			bt_controller_send(1);
// 			return false;
// 		}
	
	return true;
}

/*
SD_MODEL sd_model = {
	spi_init,
	sd_state_recognize,
	sd_clear_main_sector,
	sd_write_block_data,
	sd_read_block_data,
	sd_add_sector_data,
	false,
	{
		SD_UNKNOWN
	}
};
*/