#ifndef SD_STORAGE_H_
#define SD_STORAGE_H_

#include <avr/io.h>
#include <stdbool.h>
#include "sd_storage_resp.h"

#define CS (1<<PB2)
#define MOSI (1<<PB3)
#define MISO (1<<PB4)
#define SCK (1<<PB5)
#define CS_DDR DDRB
#define CS_ENABLE() (PORTB &= ~CS)
#define CS_DISABLE() (PORTB |= CS)

#define SD_ID 772236000UL
#define SIZE_SECTOR 512
#define SD_BLANK_BYTE 0x00

#define SD_SWAP_ERR 0xE8
#define SD_EMPTY 0xE9
#define SD_WRITE_ERR 0xEA
#define SD_READ_ERR 0xEB
#define SD_MAIN_SECTOR_UPDATE_ERR 0xEC


#define START_BIT (1<<7)
#define PARAMETER_ERROR (1<<6)
#define ADDRESS_ERROR (1<<5)
#define ERASE_SEQUENCE_ERROR (1<<4)
#define COM_CRC_ERROR (1<<3)
#define ILLEGAL_COMMAND (1<<2)
#define ERASE_RESET (1<<1)
#define IN_IDLE_STATE 1
#define ACTIVE_STATE 0	



//w momencie nieprzewidzianej odpowiedzi na komendy
typedef enum{
	SD_OK = 0x00, 
	SD_TO_SPI_SWITCH_ERR =	0xF0,//CMD0
	SD_VHS_GENERAL_ERR =	0xF8,//CMD8 R7
	SD_VHS_DETAILED_ERR =	0xF5,//CMD58 OCR
	SD_OUT_OF_IDLE_ERR =	0xF4,//ACMD41 (v2.0)
	SD_NOT_SUPPORTED_CARD =	0xF1,//ACMD41 err
	SD_UNEXPECTED_ERR =		0xFF //not respond
}SD_STATUS_COMM;

typedef enum{
	SD_gt_v20 = 0x20,
	SD_v1x = 0x10
}SD_VERSION;

typedef enum{
	SDHC = 0x01,
	SDSC = 0x00
}SD_TYPE;

typedef enum{
	SD_INIT = 0xE1, //all OK
	SD_NOT_INIT = 0xE2, //bad SD_ID
	SD_FAILED = 0xE3, //unexpected error (remove card or electrical issue)
	SD_ABSENT = 0xE4, //0xF0 comm resp
	SD_UNSUPPORTED = 0xE5, //0xF8, 0xF5, 0xF4, 0xF1 comm resp
	SD_OUT_OF_SYNC = 0xE6, //not sync main sector card with data in sram
	SD_UNKNOWN = 0xE7//on start
}SD_STATUS;

typedef struct{
	SD_STATUS status;
	SD_VERSION version;
	SD_TYPE type;
	uint32_t sectors_cap;
	uint32_t sectors_occp;
	uint32_t current_sector;
	bool is_new;
}SD_INFO;

typedef union{
	uint8_t byte[8];
	struct{
		uint32_t id;
		uint32_t current_sector; //ostatni w ktory cos sie zmiesci
	};
}SD_MAIN_SECTOR;

#define MEAS_PORTION_DATA_SIZE_BYTES 16

extern bool sd_model_is_overflow;
extern SD_INFO sd_model_info;
extern uint8_t sd_model_buffer[SIZE_SECTOR];

void spi_init();
bool sd_model_state_recognize();
bool sd_model_write_main_sector(SD_MAIN_SECTOR main_sector);
bool sd_model_clear_main_sector();
bool sd_model_write_sector(uint32_t sector, uint8_t * data, uint16_t start_pos, uint16_t bytes_amount);
bool sd_model_read_sector(uint32_t sector, uint8_t * data, uint16_t start_pos, uint16_t bytes_amount);
bool sd_model_add_sector_data(uint8_t * data);

// typedef struct{
// 	void (*init_spi)();
// 	bool (*state_recognize)();
// 	bool (*clear_main_sector)();
// 	bool (*write_sector)(uint32_t sector, uint8_t * data);
// 	bool (*read_sector)(uint32_t sector, uint8_t * data);
// 	bool (*add_sector_data)(uint8_t * data);
// 	bool is_overflow;
// 	SD_INFO info;
// 	uint8_t buffer[SIZE_SECTOR];
// }SD_MODEL;
// 
// extern SD_MODEL sd_model;

#endif /* SD_STORAGE_H_ */