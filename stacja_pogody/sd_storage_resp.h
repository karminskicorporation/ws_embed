#ifndef SD_STORAGE_RESP_H_
#define SD_STORAGE_RESP_H_

typedef union{
	uint8_t byte;
	struct{
		bool
		in_idle_state:1,		//LSB
		erase_reset:1,
		illegal_command:1,
		com_crc_error:1,
		erase_sequence_error:1,
		address_error:1,
		parameter_error:1,
		start_bit:1; //always 0	//MSB
	};
}SD_R1;

typedef union{
	uint8_t byte[2];
	uint16_t all;
	struct{
		bool
		card_is_locked:1,
		wp_erase_skip_or_lock_unlock_cmd_failed:1,
		error:1,
		cc_error:1,
		card_ecc_failed:1,
		wp_violation:1,
		erase_param:1,
		out_of_range_or_csd_overwrite:1;
		SD_R1 r1;
	};
}SD_R2;

typedef union{
	uint8_t byte[5];
	struct{
		uint32_t other:32;//R7 DWORD little endian
		uint8_t :8;     //R1 union MSB
	};
	struct{
		uint8_t check_pattern:8;    //bits 7-0
		uint8_t voltage_accepted:4; //bits 11-8
		uint8_t :4; //reserved      //bits 15-12,   union BYTE complement
		uint8_t :8; //reserved      //bits 23-16,   union BYTE complement
		uint8_t :4; //reserved      //bits 27-24,   union BYTE complement
		uint8_t command_version:4;  //bits 31-28
		SD_R1 r1;
	};
}SD_R7;

typedef union{
	uint8_t byte[4];
	uint32_t all;
	struct{
		uint8_t :8;
		uint8_t :7;
		bool
		v27_28:1,
		v28_29:1,
		v29_30:1,
		v30_31:1,
		v31_32:1,
		v32_33:1,
		v33_34:1,
		v34_35:1,
		v35_36:1;
		bool S18A:1;
		uint8_t :4;
		bool uhs_ii_card_status:1;
		bool ccs:1;
		bool power_up_status:1;
	};
}SD_OCR;

#define C_SIZE_MULT_v10(csd) ((((csd).c_size_mult_H)<<1) | ((csd).c_size_mult_L))
#define C_SIZE_v10(csd) ((((csd).c_size_H)<<2) | ((csd).c_size_L))

typedef union{
	uint8_t byte[16];
	struct{
		uint16_t
		:1,
		crc :7,
		:2,
		file_format :2,
		tmp_write_protect :1,
		perm_write_protect :1,
		copy :1,
		file_format_grp :1,
		:5,
		write_bl_partial :1,
		write_bl_len :4,
		r2w_factor :3,
		:2,
		wp_grp_enable :1,
		wp_grp_size :7,
		sector_size :7,
		erase_blk_en :1,
		c_size_mult_L :1,
		c_size_mult_H :2,
		vdd_w_curr_max :3,
		vdd_w_curr_min :3,
		vdd_r_curr_max :3,
		vdd_r_curr_min :3,
		c_size_L :2,
		c_size_H :10,
		:2,
		dsr_imp :1,
		read_blk_misalign :1,
		write_blk_misalign :1,
		read_bl_partial :1,
		read_bl_len :4,
		ccc :12,
		tran_speed: 8,
		nsac :8,
		taac :8,
		:6,
		csd_structure :2;
	};
}SD_CSD_v10;

#define C_SIZE_v20(csd) (((uint32_t)((csd).c_size_H)<<16) | ((csd).c_size_L))

typedef union{
	uint8_t byte[16];
	struct{
		uint16_t
		:1,
		crc :7,
		:2,
		file_format :2,
		tmp_write_protect :1,
		perm_write_protect :1,
		copy :1,
		file_format_grp :1,
		:5,
		write_bl_partial :1,
		write_bl_len :4,
		r2w_factor :3,
		:2,
		wp_grp_enable :1,
		wp_grp_size :7,
		sector_size :7,
		erase_blk_en :1,
		:1,
		c_size_L :16,
		c_size_H :6,
		:6,
		dsr_imp :1,
		read_blk_misalign :1,
		write_blk_misalign :1,
		read_bl_partial :1,
		read_bl_len :4,
		ccc :12,
		tran_speed: 8,
		nsac :8,
		taac :8,
		:6,
		csd_structure :2;
	};
}SD_CSD_v20;



#endif /* SD_STORAGE_RESP_H_ */