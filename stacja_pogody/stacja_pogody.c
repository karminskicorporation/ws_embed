#include <avr/io.h>
#include <avr/interrupt.h>

#include "rtc_meas.h"
#include <util/delay.h>
#include <stdbool.h>
#include "stdlib.h"
#include "twisup.h"
#include "lcdsup.h"
#include "weather_data.h"
#include "power_supply.h"
#include "text_set.h"
#include "bt_usart.h"
#include "refresh_control.h"
#include "AM2320.h"
#include "sd_storage.h"
#include "byte_conversion.h"
#include "BMP180.h"

#define MAINS_LED_INIT DDRD |= (1<<PD4)
#define MAINS_LED_ON PORTD |= (1<<PD4)
#define MAINS_LED_OFF PORTD &= ~(1<<PD4)
#define MAINS_LED_SWITCH PORTD ^= (1<<PD4)

#define BATT_LED_INIT DDRD |= (1<<PD5)
#define BATT_LED_ON PORTD |= (1<<PD5)
#define BATT_LED_OFF PORTD &= ~(1<<PD5)
#define BATT_LED_PORT PORTD
#define BATT_LED_PIN PD5

typedef enum{
	KEEP_RTC_STATE,
	COMMON_STATE
}DEVICE_STATE;


int main(void){	
	DEVICE_STATE current_state;
	//SPI_SD_STATUS sd_status;
	//SD_R2 r2;
	uint32_t i;
	//uint8_t response[64];
	//uint8_t control;
	//DWORD_TO_BYTE dw2b;
	
	DDRD |= (1<<PD7);
	DDRB |= (1<<0);
	
	sei();
	PORTC |= (1<<PC5) | (1<<PC4); //TWI
	twi_controller_set_clk(50);
	alcd_switch_backlight(false);
	twi_controller_off();
	MAINS_LED_INIT;
	BATT_LED_INIT;
	AM2320_DDR |= (1<<AM2320_PIN);
	rtc_meas_init(100);
	//rtc_meas_init_match(100);
	rtc_meas_check_voltage_flag = true;
	current_state = KEEP_RTC_STATE;
	
	while(true){
		if(rtc_meas_check_voltage_flag){
			rtc_meas_check_voltage_flag=false;
			
			//spr stanu zasilania (po wybudzeniu przez COMP2)
			ps_controller_init();
			ps_controller_get_voltage();
			ps_controller_set_state();
			ps_controller_disable();
			
			if(ps_controller_power_type == POWER_BATTERY){
				if(current_state == COMMON_STATE){ //wyl
					alcd_switch_backlight(false);
					MAINS_LED_OFF;
					bt_controller_disable();
					//wyl HC-05 enable
					//wyl czujnikow
					AM2320_PORT &= ~(1<<AM2320_PIN);
					twi_controller_off();
					//wyl SD
					ps_controller_disable();
					refresh_control_disable();
					
					rtc_meas_check_voltage_flag = false;
					//rtc_meas.sec_flag = false;
					//odczyt czujnikow
					//odswiezenie ekranu
					//przycisk choose
					//przycisk gora
					//przycisk dol
					//komenda USART
					
					current_state = KEEP_RTC_STATE;
				}
			}else if(ps_controller_power_type == POWER_MAIN){
				if(current_state == KEEP_RTC_STATE){ //wl
					//PORTC |= (1<<PC5) | (1<<PC4); //TWI
					twi_controller_set_clk();
					alcd_switch_backlight(true);
					//if(!(alcd.is_init))
					alcd_init();
					MAINS_LED_ON;
					bt_controller_init();
					//wl HC-05 enable
					//wl czujnikow
					AM2320_PORT |= (1<<AM2320_PIN);
					if(!BMP180_is_init){
						BMP180_get_calib();
						BMP180_is_init = true;
					}
					spi_init();//wl SD
					sd_model_state_recognize();//check SD
					CS_DISABLE();
					refresh_control_init(20, 1);
					
					current_state = COMMON_STATE;
					refresh_control_take_meas_flag = true;
				}
			}
			
			
			switch(ps_controller_battery_state){
				case BATTERY_CRITICAL:
					ps_controller_init_blinking(1, 3);
					ps_controller_blink_sec(BATT_LED_PORT, BATT_LED_PIN);
					break;
				case BATTERY_TO_HIGH:
					ps_controller_init_blinking(1, 1);
					ps_controller_blink_sec(BATT_LED_PORT, BATT_LED_PIN);
					break;
				
				case BATTERY_LOW:
					ps_controller_init_blinking(3, 1);
					ps_controller_blink_sec(BATT_LED_PORT, BATT_LED_PIN);
					break;
				case BATTERY_OK:
					BATT_LED_ON;
					break;
			}	
			
			
			if(current_state == KEEP_RTC_STATE && false){
				ps_controller_set_sleep();
				continue;
			}
		}  //end rtc_meas.match_flag
		
		if(bt_controller_state == BT_PROCESSING){
			DWORD_TO_BYTE dnum_buffer;
			WORD_TO_BYTE num_buffer;			
			
			switch(bt_controller_command_buffer[0]){
				
				case 0x20:	//' '
				if(sd_model_state_recognize()){
					//error code
					bt_controller_response_buffer[0] = BT_OK;
					//p1 data size in bytes
					dnum_buffer.dword = ((sd_model_info.sectors_occp)<<1) | (((weather_data_offset)&0x100)>>8);
					for(i=1; i<5; i++) bt_controller_response_buffer[i] = dnum_buffer.byte[3-(i-1)];
					bt_controller_response_buffer[5] = (weather_data_offset)&0xFF;
					//p2 memory capacity in bytes
					dnum_buffer.dword = (sd_model_info.sectors_cap)<<1;
					for(i=6; i<10; i++) bt_controller_response_buffer[i] = dnum_buffer.byte[3-(i-6)];
					bt_controller_response_buffer[10] = 0x00;
					//p3 measurment data portion in number of bytes
					bt_controller_response_buffer[11] = MEAS_PORTION_DATA_SIZE_BYTES;
					//p4 measurment trigger time period in second
					num_buffer.word = refresh_control_meas_match_ms/1000;
					bt_controller_response_buffer[12] = num_buffer.byte[1];
					bt_controller_response_buffer[13] = num_buffer.byte[0];
					bt_controller_send(14);
				}else{
					bt_controller_response_buffer[0] = sd_model_info.status;
					bt_controller_send(1);
				}
				break;
				
// 				case 0x22: //'"'
// 				--(weather_data_portion_iterator);
// 				//data portion send again without break provides code saving
							
				case 0x21: //'!'
				
					//sektor 
					for(i=0; i<4; i++) dnum_buffer.byte[3-i] = bt_controller_command_buffer[i+1];
					//offset
					num_buffer.byte[1] = bt_controller_command_buffer[5];
					num_buffer.byte[0] = bt_controller_command_buffer[6];
					
					//spr czy poprawny sektor
					if(dnum_buffer.dword == 0){
						//spr czy poprawny offset
						if(num_buffer.word + BT_DATA_PORTION_SIZE_B <= weather_data_offset){
							bt_controller_response_buffer[0] = BT_OK;
							for(i=0; i<64; i++) bt_controller_response_buffer[i+1] = sd_model_buffer[num_buffer.word + i];
							bt_controller_send(65);
							break;
						}	
					}else if(dnum_buffer.dword < sd_model_info.current_sector){
						//spr czy poprawny offset
						if(num_buffer.word + BT_DATA_PORTION_SIZE_B <= SIZE_SECTOR){
							//pobranie czesci sektora do bluetootha
							if(!sd_model_state_recognize() ||
							!sd_model_read_sector(dnum_buffer.dword, (uint8_t *)bt_controller_response_buffer+1, num_buffer.word, BT_DATA_PORTION_SIZE_B)){
								bt_controller_response_buffer[0] = sd_model_info.status;
								bt_controller_send(1);
								break;
							}else{ //Ok
								bt_controller_response_buffer[0] = BT_OK;
								bt_controller_send(65);
								break;
							}
						}
					}
					//err out of range
					bt_controller_response_buffer[0] = BT_PARAM_OUT_OF_RANGE;
					bt_controller_send(1);
					break;

// 						uint32_t collected_sd_portion = sd_model_info.sectors_occp*BT_COEF_SD_SECTOR_BT_PORTION;
// 						uint32_t collected_data_portion = collected_sd_portion + weather_data_offset/BT_DATA_PORTION_SIZE_B;
// 					
// 					if(weather_data_portion_iterator + 1 > collected_data_portion ){ //no data for send
// 							bt_controller_response_buffer[0] = BT_NO_MORE_FOR_SEND; //p0 -- err
// 							bt_controller_send(1); 
// 							break;
// 					}
// 				 
// 					//p3
// 					if(weather_data_portion_iterator + 1 <= collected_sd_portion){ //data in sd card
// 						uint16_t transmit_start = BT_DATA_PORTION_SIZE_B*(weather_data_portion_iterator%BT_COEF_SD_SECTOR_BT_PORTION); 
// 						uint32_t sector_to_read = weather_data_portion_iterator/BT_COEF_SD_SECTOR_BT_PORTION + 1;
// 					
// 						//pobranie danych z karty sd 
// 						if(!sd_model_state_recognize() ||
// 						   !sd_model_read_sector(sector_to_read, (uint8_t *)bt_controller_response_buffer+6, transmit_start, BT_DATA_PORTION_SIZE_B)){
// 							bt_controller_response_buffer[0] = sd_model_info.status;
// 							bt_controller_send(1);
// 							break;
// 						}
// 					
// 					}else{ //data in sram
// 						uint16_t transmit_start = (weather_data_portion_iterator - collected_sd_portion)*BT_DATA_PORTION_SIZE_B;
// 					
// 						for(i=6; i<6+BT_DATA_PORTION_SIZE_B; i++)
// 							bt_controller_response_buffer[i] = sd_model_buffer[i-6+transmit_start];
// 					}
// 		
// 					bt_controller_response_buffer[0] = BT_OK; //p0 -- OK
// // 					bt_controller_response_buffer[1] = BT_DATA_PORTION_SIZE_B; //p1
// // 					//data remaind calculation
// // 					dnum_buffer.dword = collected_data_portion-weather_data_portion_iterator;
// // 					for(i=2; i<6; i++) bt_controller_response_buffer[i] = dnum_buffer.byte[3-(i-2)]; //p2
// 					bt_controller_send(6+BT_DATA_PORTION_SIZE_B);
// 					++weather_data_portion_iterator;

				break;
				
// 				case 0x23: //'#'
// 				weather_data_portion_iterator = 0;
// 				bt_controller_response_buffer[0] = BT_OK;
// 				bt_controller_send(1);
// 				break;	
		
				/*
				case 0x24: //'$'
				if(sd_model_state_recognize() && sd_model_info.status == SD_INIT && sd_model_clear_main_sector()){
					weather_data_portion_iterator = 0;
					bt_controller_response_buffer[0] = BT_OK;					
				}else
					bt_controller_response_buffer[0] = sd_model_info.status;
				bt_controller_send(1);
				break;
				*/				
				
				case 0x25: //'%'
				bt_controller_response_buffer[0] = BT_OK;
				num_buffer.sword = AM2320_temperature;
				bt_controller_response_buffer[1] = num_buffer.byte[1];
				bt_controller_response_buffer[2] = num_buffer.byte[0];
				num_buffer.sword = BMP180_temperature;
				bt_controller_response_buffer[3] = num_buffer.byte[1];
				bt_controller_response_buffer[4] = num_buffer.byte[0];
				num_buffer.word = AM2320_humidity;
				bt_controller_response_buffer[5] = num_buffer.byte[1];
				bt_controller_response_buffer[6] = num_buffer.byte[0];
				num_buffer.word = BMP180_pressure;
				bt_controller_response_buffer[7] = num_buffer.byte[1];
				bt_controller_response_buffer[8] = num_buffer.byte[0];
				bt_controller_send(9);
				break;
				/*
				case 0x26: //'&'
				num_buffer.word = ps_controller_r_batt;
				bt_controller_response_buffer[0] = num_buffer.byte[1];
				bt_controller_response_buffer[1] = num_buffer.byte[0];
				num_buffer.word = ps_controller_r_mains;
				bt_controller_response_buffer[2] = num_buffer.byte[1];
				bt_controller_response_buffer[3] = num_buffer.byte[0];
				bt_controller_send(4);
				break;
				*/
				
				case 0x27: //'''							
				rtc_meas_is_reset = false;
				for(i=0; i<4; i++)
					dnum_buffer.byte[3-i] = bt_controller_command_buffer[i+1];
				rtc_meas_timestamp = dnum_buffer.dword;
				bt_controller_response_buffer[0] = BT_OK;
				
				bt_controller_send(1);
				break;
/*
				case 0x28: //'('
				if(!(sd_model_state_recognize()) && sd_model_info.status == SD_NOT_INIT && sd_model_clear_main_sector()){
					weather_data_portion_iterator = 0;
					bt_controller_response_buffer[0] = BT_OK;
				}else
					bt_controller_response_buffer[0] = sd_model_info.status;
				bt_controller_send(1);
				break;
*/			
// 				case 0x29: //')'
// 				dnum_buffer.dword = rtc_meas_timestamp;
// 				bt_controller_response_buffer[0] = dnum_buffer.byte[3];
// 				bt_controller_response_buffer[1] = dnum_buffer.byte[2];
// 				bt_controller_response_buffer[2] = dnum_buffer.byte[1];
// 				bt_controller_response_buffer[3] = dnum_buffer.byte[0];
// 				bt_controller_send(4);
// 				break;
						
				default:
				bt_controller_response_buffer[0] = BT_PARAM_ERR;
				bt_controller_send(1);
				break;
			}
			bt_controller_state = BT_STANDBY;
		}else  //end bt_controller processing
			
		if(bt_controller_state == BT_REQUEST){
			switch(bt_controller_command_buffer[0]){
				case 0x27:
				bt_controller_order(4);
				break;
				case 0x21:
				bt_controller_order(6);
				break;
				//case 0x29:
				//break;
				default:
				bt_controller_order(0);
			}
		} //end bt_controller ordering
		
			
// 		if(rtc_meas.sec_flag){
// 			DWORD_TO_BYTE dw2b;
// 			dw2b.dword = rtc_meas.timestamp;
// 			for(i=0; i<4; i++) bt_controller.response_buffer[i] = dw2b.byte[3-i];
// 			bt_controller.send(4);
// 			rtc_meas.sec_flag = false;	
// 		} //end sec
		if(refresh_control_take_meas_flag){
			//PORTD |= (1<<PD7);
			//_delay_ms(10);
			//PORTD &= ~(1<<PD7);
			if(!AM2320_get_all()){
				//restart power if failed
				AM2320_PORT &= ~(1<<AM2320_PIN);
				_delay_ms(1);
				AM2320_PORT |= (1<<AM2320_PIN);
			}
			//BMP180
			BMP180_meas();
			
			weather_data_add_to_sd_buffer();
			
			if(weather_data_offset >= SIZE_SECTOR){
							
				if(sd_model_add_sector_data(sd_model_buffer))
					weather_data_offset = 0;
				else{
					//shift data
					for(i=0; i<SIZE_SECTOR-MEAS_PORTION_DATA_SIZE_BYTES; i++)
						sd_model_buffer[i] = sd_model_buffer[i+MEAS_PORTION_DATA_SIZE_BYTES];
					weather_data_offset -= MEAS_PORTION_DATA_SIZE_BYTES;
				}
			}
			refresh_control_take_meas_flag = false;
		}  //end take meas 
		if(refresh_control_refresh_lcd_flag){
			weather_data_display();
			refresh_control_refresh_lcd_flag = false;
		}  //end lcd refresh	
		
		_delay_us(1);	
	}  //end infinity loop!!!
	
	
	return 0;
} 
