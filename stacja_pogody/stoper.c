#include "stoper.h"
#include <stdbool.h>
#include <avr/io.h>
#include <avr/interrupt.h>

uint32_t stoper_timestamp_ms = 0;
uint16_t stoper_timestamp_us = 0;
bool stoper_is_init = false;
bool stoper_is_on = false;
uint32_t stoper_match_ms = 0;

void stoper_init(uint32_t ms){
	TCNT0 = 0;
	stoper_timestamp_us = 0;
	stoper_timestamp_ms = 0;
	if(!(stoper_is_init)){
		TCCR0 = (1<<CS00);
		TIMSK |= (1<<TOIE0);
		stoper_is_init = true;
	}
	stoper_match_ms = ms;
	stoper_is_on = true;
}

bool stoper_catch(){
	if(stoper_timestamp_ms>stoper_match_ms && stoper_is_on){
		stoper_is_on = false;
		return true;
	}else
		return false;
	
}



// STOPER stoper = {
// 	stoper_init,
// 	stoper_catch,
// 	0,
// 	0,
// 	false,
// 	false,
// 	0
// };

ISR(TIMER0_OVF_vect){
	stoper_timestamp_us += 256;
	if(stoper_timestamp_us>1000){
		stoper_timestamp_us -= 1000;
		++(stoper_timestamp_ms);
	}
}