#ifndef STOPER_H_
#define STOPER_H_

#include <stdbool.h>
#include <avr/io.h>

extern  uint32_t stoper_timestamp_ms;
extern uint16_t stoper_timestamp_us;
extern bool stoper_is_init;
extern bool stoper_is_on;
extern uint32_t stoper_match_ms;

void stoper_init(uint32_t ms);
bool stoper_catch();

// typedef struct{
// 	void (*init)(uint32_t ms);
// 	bool (*catch_t)();
// 	uint32_t timestamp_ms;
// 	uint16_t timestamp_us;
// 	bool is_init;
// 	bool is_on;
// 	uint32_t match_ms;
// }STOPER;

//extern STOPER stoper;

#endif /* STOPER_H_ */