#include <avr/io.h>
#include "text_set.h"
//#include "lcdsup.h"


// void get_digit_to_char(char * sign, uint16_t number, uint8_t digit_pos){
// 	uint16_t modulo10 = 1;
// 	uint8_t i;
// 	for(i=0; i<digit_pos; i++)
// 		modulo10 *= 10;
// 	
// 	(*sign) = 0x30 + (uint8_t)((number%modulo10-number%(modulo10/10))/(modulo10/10));
// }

static uint32_t pow_dec(uint8_t exp){
	uint8_t i;
	uint32_t number = 1;
	for(i=0; i<exp; i++) number *= 10;
	return number;
}

void number2string(char * str, uint32_t number, uint8_t digit_n, uint8_t point_pos){
	bool is_digit = false;
	uint8_t i, sign;
	uint32_t divisor = pow_dec(digit_n-1);
	for(i=0; i<digit_n; i++, divisor/=10){
		if(i==digit_n-point_pos+1) *str++ = ',';
		sign = 0x30 + (uint8_t)((number/divisor)%10);
		if(!is_digit && sign=='0')
		*str++ = ' ';
		else{
			*str++ = 0x30 + (uint8_t)((number/divisor)%10);
			is_digit = true;
		}
	}
}

void text_set_temperature(char * text, uint16_t temperature){
	text[0] = 'I';
	text[1] = ':';
	number2string(text+2, temperature, 3, 2);
// 	get_digit_to_char(&(text[2]), temperature, 3);
// 	if(text[2]=='0') text[2]=' ';
// 	get_digit_to_char(&(text[3]), temperature, 2);
// 	text[4] = '.';
// 	get_digit_to_char(&(text[5]), temperature, 1);
	text[6] = 'C';
}

void text_set_temperature_p_m(char * text, bool is_minus, uint32_t temperature){
	text[0] = 'O';
	text[1] = ':';
	text[2] = (is_minus ? '-' : ' ');
	number2string(text+3, temperature, 3, 2);
// 	get_digit_to_char(&(text[3]), temperature, 3);
// 	if(text[3]=='0') text[3]=' ';
// 	get_digit_to_char(&(text[4]), temperature, 2);
// 	text[5] = '.';
// 	get_digit_to_char(&(text[6]), temperature, 1);
	text[7] = 'C';
}

void text_set_humidity(char * text, uint16_t humidity){
	number2string(text, humidity, 4, 2);
// 	get_digit_to_char(&(text[0]), humidity, 4);
// 	if(text[0]=='0') text[0]=' ';
// 	get_digit_to_char(&(text[1]), humidity, 3);
// 	if(text[0]=='0' && text[1]=='0') text[1]=' ';
// 	get_digit_to_char(&(text[2]), humidity, 2);
// 	text[3] = '.';
// 	get_digit_to_char(&(text[4]), humidity, 1);
	text[5] = '%';
}

void text_set_pressure(char * text, uint32_t pressure){
	number2string(text, pressure, 5, 2);
// 	get_digit_to_char(&(text[0]), pressure, 5);
// 	if(text[0]=='0') text[0]=' ';
// 	get_digit_to_char(&(text[1]), pressure, 4);
// 	get_digit_to_char(&(text[2]), pressure, 3);
// 	get_digit_to_char(&(text[3]), pressure, 2);
// 	text[4] = '.';
// 	get_digit_to_char(&(text[5]), pressure, 1);
	text[6] = 'h';
	text[7] = 'P';
	text[8] = 'a';
}

/*
void text_set_time_date(char* text_time, char* text_date, uint32_t timestamp){
	uint8_t sec=0, min=0, hour=0, day=1, month=1;
	uint16_t year, day_sum=0;
	uint32_t rest = timestamp;
	bool leap_year;
	
	for(year=1970; ;year++){ //lata
		if ((year % 4 == 0) && (year % 100 != 0) || (year % 400 == 0)){ //rok przestepny?
			if(rest<366UL*24*60*60){
				leap_year=true;
				break;
			}
			else
			rest -= 366UL*24*60*60;
		}
		else{
			if(rest<365UL*24*60*60){
				leap_year=false;
				break;
			}
			else
			rest -= 365UL*24*60*60;
		}
	}
	
	for(month=1; ;month++){ //miesiace
		uint16_t  day_sum_before = day_sum;
		switch(month){
			case 1:
			case 3:
			case 5:
			case 7:
			case 8:
			case 10:
				day_sum+=31;
				break;
			case 2:
				if(leap_year) day_sum+=29;
				else day_sum+=28;
				break;
			default:
				day_sum+=30;
		}
		if(rest < (uint32_t)day_sum*24*60*60){
			rest -= (uint32_t)day_sum_before*24*60*60;
			break;
		}
	}
	
	day = rest/60/60/24;
	rest -= (uint32_t)day*60*60*24;

	hour = rest/60/60;
	rest -= (uint32_t)hour*60*60;

	min = rest/60;
	rest -= (uint32_t)min*60;

	sec = rest;
	
	get_digit_to_char(text_date++, day, 2);
	get_digit_to_char(text_date++, day, 1);
	*text_date++ = '-';
	get_digit_to_char(text_date++, month, 2);
	get_digit_to_char(text_date++, month, 1);
	*text_date++ = '-';
	get_digit_to_char(text_date++, year, 4);
	get_digit_to_char(text_date++, year, 3);
	get_digit_to_char(text_date++, year, 2);
	get_digit_to_char(text_date++, year, 1);
	
	get_digit_to_char(text_time++, hour, 2);
	if(*text_time=='0') *text_time=' ';
	get_digit_to_char(text_time++, hour, 1);
	*text_time++ = ':';
	get_digit_to_char(text_time++, min, 2);
	get_digit_to_char(text_time++, min, 1);
	*text_time++ = ':';
	get_digit_to_char(text_time++, sec, 2);
	get_digit_to_char(text_time++, sec, 1);
	
}
*/


// TEXT_SET text_set = {
// 	text_set_temperature,
// 	text_set_temperature_p_m,
// 	text_set_humidity,
// 	text_set_pressure//,
// 	//text_set_time_date,
// 	//text_set_split_command,
// 	//text_set_comp_rnstrings
// };