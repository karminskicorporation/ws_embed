#ifndef TEXT_SET_H_
#define TEXT_SET_H_

#include <stdbool.h>

void text_set_temperature(char * text, uint16_t temperature);
void text_set_temperature_p_m(char * text, bool is_minus, uint32_t temperature);
void text_set_humidity(char * text, uint16_t humidity);
void text_set_pressure(char * text, uint32_t pressure);

// typedef struct{
// 	void (*temperature)(char * text, uint16_t temperature);
// 	void (*temperature_p_m)(char * text, bool is_minus, uint16_t temperature);
// 	void (*humidity)(char * text, uint16_t humidity);
// 	void (*pressure)(char * text, uint16_t pressure);
// 	//void (*time_date)(char* text_time, char* text_date, uint32_t timestamp);
// 	//bool (*split_command)(char * str, char * name, char ** params, uint8_t * n_params, bool * is_getter);
// 	//bool (*comp_rnstrings)(char * str1, char * str2);
// }TEXT_SET;

//extern TEXT_SET text_set;

#endif /* TEXT_SET_H_ */