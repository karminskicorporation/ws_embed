#include <util/delay.h>
#include "twisup.h"
#include "stoper.h"

//pre function

void twi_start(){
	//stoper_init(50);
	uint32_t i=0;
	TWCR = (1<<TWINT) | (1<<TWSTA) | (1<<TWEN);
	while(!(TWCR&(1<<TWINT)) && i < 80/*!(stoper_catch())*/){ _delay_us(10); i++;}; //1us
}

void twi_send_byte(uint8_t byte){
	uint32_t i=0;
	TWDR = byte;
	TWCR = (1<<TWINT) | (1<<TWEN);
	while(!(TWCR&(1<<TWINT)) && i < 80/*!(stoper_catch())*/){ _delay_us(10); i++;}; //1us
}

//function

bool twi_controller_start(){
	twi_start();
	return TW_STATUS == TW_START;
}

bool twi_controller_restart(){
	twi_start();
	return TW_STATUS == TW_REP_START;
}

bool twi_controller_send_sla(uint8_t sla){
	twi_send_byte(sla);
	return TW_STATUS == (sla & 0x01 ? TW_MR_SLA_ACK : TW_MT_SLA_ACK);
}

bool twi_controller_send_byte(uint8_t byte){
	twi_send_byte(byte);
	return TW_STATUS == TW_MT_DATA_ACK;
}

uint8_t twi_controller_get_byte(bool is_ack){
	//stoper_init(50);
	uint32_t i=0;
	TWCR = (
		is_ack 
		? ((1<<TWINT) | (1<<TWEN) | (1<<TWEA))
		: ((1<<TWINT) | (1<<TWEN))
	);
	while(!(TWCR&(1<<TWINT)) && i < 80/*!(stoper_catch())*/){ _delay_us(10); i++; }; //1us
	return TWDR;
}

void twi_controller_stop(){
	//stoper_init(50);
	uint32_t i=0;
	TWCR = (1<<TWINT) | (1<<TWSTO) | (1<<TWEN);
	while(TW_STATUS!=TW_NO_INFO  && i < 80/*!(stoper_catch())*/){ _delay_us(10); i++; }; //1us
}

void twi_controller_set_clk(){
	#if F_CPU == 1000000
		TWBR = 2;
	#elif F_CPU == 8000000
		TWBR = 18;
		TWSR |= (1<<TWPS0);
	#else
		#error "Niezgodna czestotliwosc taktowania"
	#endif
}

void twi_controller_off(){
	TWCR &= ~(1<<TWEN);
}

// TWI_CONTROLLER twi_controller = {
// 	twi_set_clk,
// 	twi_start,
// 	twi_send_byte,
// 	twi_get_byte,
// 	twi_stop,
// 	twi_off
// };

