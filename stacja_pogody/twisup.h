#ifndef INCFILE1_H_
#define INCFILE1_H_
#include <avr/io.h>
#include <stdbool.h>
#include <util/twi.h>

void twi_controller_set_clk();
bool twi_controller_start();
bool twi_controller_restart();
bool twi_controller_send_byte(uint8_t byte);
bool twi_controller_send_sla(uint8_t sla);
uint8_t twi_controller_get_byte(bool is_ack);
void twi_controller_stop();
void twi_controller_off();

// typedef struct{
// 	void (*set_clk)();
// 	void (*start)();
// 	void (*send_byte)(uint8_t byte);
// 	uint8_t (*get_byte)(bool is_ack);
// 	void (*stop)();
// 	void (*off)();
// }TWI_CONTROLLER;
// 
// extern TWI_CONTROLLER twi_controller;

#endif /* INCFILE1_H_ */