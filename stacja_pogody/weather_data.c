#include "weather_data.h"
#include "AM2320.h"
#include "BMP180.h"
#include "lcdsup.h"
#include <avr/io.h>
#include <stdbool.h>
//#include "text_set.h"
#include "sd_storage.h"
#include "rtc_meas.h"
#include "byte_conversion.h"

#include <stdio.h>

uint16_t weather_data_offset = 0;
//min 16bit portion provides 64GB support card
uint32_t weather_data_portion_iterator = 0;

static uint32_t pow_dec(uint8_t exp){
	uint8_t i;
	uint32_t number = 1;
	for(i=0; i<exp; i++) number *= 10;
	return number;
}

static void number2string(char * str, uint32_t number, uint8_t digit_n, uint8_t point_pos){
	bool is_digit = false;
	uint8_t i, sign;
	
	uint32_t divisor = pow_dec(digit_n-1);
	for(i=0; i<digit_n; i++, divisor/=10){
		if(i==digit_n-point_pos+1) *str++ = ',';
 		sign = number/divisor;
 		sign = 0x30 + sign - 10*(sign/10); 
		if(!is_digit && sign=='0' && i<digit_n-point_pos)
			*str++ = ' ';
		else{
			*str++ = sign;
			is_digit = true;
		}
	}
}

void weather_data_display(){
	
	alcd_str2line(false, "O:     C I:    C");
	alcd_str2line(true, "     %       hPa");
	
 	alcd_line[0][2] = (AM2320_temperature<0 ? '-' : ' ');
	number2string(alcd_line[0]+9+2, BMP180_temperature, 3, 2);
	number2string(alcd_line[0]+3, (uint16_t)((AM2320_temperature<0 ? (-1) : 1) * AM2320_temperature), 3, 2);
	number2string(alcd_line[1], AM2320_humidity, 4, 2);
	number2string(alcd_line[1]+7, BMP180_pressure, 5, 2);
	
	alcd_print(0, 0);
	alcd_print(1, 0);
};

//#DEBUG
/*#include "bt_usart.h"*/


bool weather_data_add_to_sd_buffer(){
// 	uint32_t i;
// 	union{
// 		uint8_t byte[16];
// 		struct{
// 			uint8_t reserved[2];
// 			uint16_t ms;
// 			uint32_t timestamp;
// 			uint16_t pressure;
// 			uint16_t humidity;
// 			uint16_t itemp;
// 			uint16_t otemp;
// 		};
// 	}conv;
	
	if(weather_data_offset + MEAS_PORTION_DATA_SIZE_BYTES > SIZE_SECTOR)
		return false;
	uint8_t * vector = sd_model_buffer + weather_data_offset;
	WORD_TO_BYTE w2b;
	DWORD_TO_BYTE dw2b;
	
// 	conv.otemp = (AM2320_is_negative_temperature ? (-1) : (1))*AM2320_temperature;
// 	conv.itemp = 0;
// 	conv.humidity = AM2320_humidity;
// 	conv.pressure = 0;
// 	conv.timestamp = rtc_meas_timestamp;
// 	conv.ms = rtc_meas_get_ms();
// 	conv.reserved[0] = 0;
// 	conv.reserved[1] = 0;
	
// 	for(i=0; i<16; i++)
//  		*vector++ = conv.byte[15-i];

	
	//outdoor temperature
	w2b.sword = AM2320_temperature;
	*vector++ = w2b.byte[1];
	*vector++ = w2b.byte[0];
		
	//#TODO BMP180 indoor temperature
	w2b.word = BMP180_temperature;
	*vector++ = w2b.byte[1];
	*vector++ = w2b.byte[0];
	//*vector++ = 0x00;
	//*vector++ = 0x00;
		
	//humidity
	w2b.word = AM2320_humidity;
	*vector++ = w2b.byte[1];
	*vector++ = w2b.byte[0];
		
	//#TODO BMP180 pressure
	w2b.word = BMP180_pressure;
	*vector++ = w2b.byte[1];
	*vector++ = w2b.byte[0];
	//*vector++ = 0x00;
	//*vector++ = 0x00;
		
	//timestamp
	dw2b.dword = rtc_meas_timestamp;
	*vector++ = dw2b.byte[3];
	*vector++ = dw2b.byte[2];
	*vector++ = dw2b.byte[1];
	*vector++ = dw2b.byte[0];
	
// 	for(i=0; i<4; i++) bt_controller_response_buffer[i] = dw2b.byte[3-i];
// 	bt_controller_send(4);
		
	//ms 
	w2b.word = rtc_meas_get_ms();
	*vector++ = w2b.byte[1];
	*vector++ = w2b.byte[0];
	
	//reserved (crc in future)
	*vector++ = 0x00;
	*vector++ = 0x00;

	
	
	//#DEBUG
// 	for(i=0; i<16; i++) bt_controller_response_buffer[i] = *(sd_model_buffer + i + weather_data_offset);
// 	bt_controller_send(16);
// 	_delay_ms(100); 	
		
	weather_data_offset += MEAS_PORTION_DATA_SIZE_BYTES;
	return true;
}

/*
WEATHER_DISPLAY weather_data = {
	weather_display,
	weather_add_to_sd_buffer,
	0,
	0
};
*/