#ifndef WEATHER_VIEW_H_
#define WEATHER_VIEW_H_

#include <stdbool.h>
#include <avr/io.h>

extern uint16_t weather_data_offset;
//min 16bit portion provides 64GB support card
extern uint32_t weather_data_portion_iterator;

void weather_data_display();
bool weather_data_add_to_sd_buffer();

// typedef struct{
// 	void (*display)();
// 	bool (*add_to_sd_buffer)();	
// 	uint16_t offset;
// 	//min 16bit portion provides 64GB support card
// 	uint32_t portion_iterator;
// }WEATHER_DISPLAY;
// 
// extern WEATHER_DISPLAY weather_data;

#endif /* WEATHER_VIEW_H_ */